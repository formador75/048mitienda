package com.misiontic.o49mitienda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O49mitiendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(O49mitiendaApplication.class, args);
	}

}
